from subprocess import run
from os import path
from cairosvg import svg2png

LEFT = 1
RIGHT = 2
UP = 3
DOWN = 4

MASK_COUNT = 0

TMP_NAME = "tmplol.svg"

class PlayerTriedToGoThroughWallError(Exception):
	'''Raised when a player tries to move out of bounds of a grid.'''
	pass

class PlayerTriedToMoveIntoAnotherPlayerError(Exception):
	'''Raised when a player tries to move into a location occupied by another player. Does not have anything to do with :meth:`grid.Player.pre_render` lol (this is a test)'''
	pass

### Obsolete ###
# just for testing or whatever
class Player:
	'''A dummy class for representing a player that can occupy grid cells.'''
	def __init__(self, direction=UP, icon=None):
		self.direction = direction
		self.icon = icon
		self.stealth = False
	
	# the grid will call this on player objects before rendering the grid
	def pre_render(self):
		'''Called right before the containing grid is rendered to an image.'''
		pass
	
	def is_stealth(self):
		'''Returns whether the player is in stealth mode.'''
		return self.stealth
### End Obsolesence lol ###

class Glyph:
	'''Represents a glyph on a grid cell.'''
	def __init__(self, name, icon):
		self.name = name
		self.icon = icon

# here's an example of how to define a glyph
# just make a subclass of Glyph
class TestGlyph(Glyph):
	'''A dummy glyph for testing purposes.'''
	def __init__(self):
		super().__init__("Test", "icon2.png")
class TestGlyph2(Glyph):
	'''Another dummy glyph for testing purposes.'''
	def __init__(self):
		super().__init__("Test 2", "icon.png")

class Cell:
	'''Represents a cell in a grid.'''
	def __init__(self, player=None, glyphs=None):
		self.player = player
		self.glyphs = glyphs or []

# make a new empty grid
def make_grid(width=10, height=10):
	'''Create a new grid object given dimenions.'''
	grid = []
	for y in range(width):
		row = []
		for x in range(height):
			row.append(Cell())
		grid.append(row)
	return grid

# get the dimensions of a grid
def get_grid_dimensions(grid):
	'''Return the width and the height of a grid object as a tuple.'''
	height = len(grid)
	width = len(grid[0])
	return width, height

# get some cell of a grid
def get_cell(grid, x, y):
	'''Return a cell object given its coordinates and containing grid.'''
	return grid[x][y]

# get the cell a player is in
def get_player_cell(grid, player):
	'''Return the cell of a grid containing a given player object.'''
	width, height = get_grid_dimensions(grid)
	for y in range(width):
		row = grid[y]
		for x in range(height):
			cell = row[x]
			if player == cell.player: return player

# get the coordinates a player is at
def get_player_position(grid, player):
	'''Given a grid and a player, returns the grid coordinates of the cell of that grid that contains that player.'''
	x = 0
	y = 0
	for column in grid:
		for cell in column:
			try:
				if player.user_id == cell.player.user_id: return x, y
			except AttributeError: x += 1 # Caused by player being a NoneType
		y += 1
		x = 0

def get_hit_chance(grid, player, enemy):
	player_location = get_player_position(grid, player)
	enemy_location = get_player_position(grid, enemy)
	return sum([4 - abs(player_location[0] - enemy_location[0]), 4 - abs(player_location[1] - enemy_location[1])])

def get_all_player_ids(grid):
	player_ids = []
	for row in grid:
		for cell in row:
			if cell.player and cell.player.user_id not in player_ids:
				player_ids.append(cell.player.user_id)
	return player_ids

# set the player of some grid cell
def set_cell_player(grid, x, y, player):
	'''Set a grid cell's occupying player given the coordinates of the cell on the grid.'''
	get_cell(grid, x, y).player = player

# set the glyphs of some grid cell
def set_cell_glyphs(grid, x, y, glyphs):
	'''Set a grid cell's glyphs (a list) given the coordinates of the cell on the grid.'''
	get_cell(grid, x, y).glyphs = glyphs

# add a new glyph to a cell
def add_cell_glyph(grid, x, y, glyph):
	'''Add a glyph to the list of glyphs currently occupying a grid cell given the coordinates of the cell on the grid.'''
	get_cell(grid, x, y).glyphs.append(glyph)

# remove any players from some grid cell
def clear_cell_player(grid, x, y):
	'''Vacate a grid cell of any occupying players given the coordinates of that cell on the grid.'''
	set_cell_player(grid, x, y, None)

# remove all glyphs from some grid cell
def clear_cell_glyphs(grid, x, y):
	'''Vacate a grid cell of all occupying glyphs given the coordinates of that cell on the grid.'''
	set_cell_glyphs(grid, x, y, [])

# swap the player contents of two cells given coordinates
def swap_players_coordinates(grid, x1, y1, x2, y2):
	'''Given a grid and two pairs of coordinates indicating two cells on that grid, swap the player occupants of the cells.'''
	cell1 = get_cell(grid, x1, y1)
	cell2 = get_cell(grid, x2, y2)
	swap_players(grid, cell1, cell2)

# swap the player contents of two cells
def swap_players(grid, cell1, cell2):
	'''Given two grid cells, swap the player occupants of the cells.'''
	p1 = cell1.player
	p2 = cell2.player
	cell1.player = p2
	cell2.player = p1

# get the cell to the left of another cell
def get_left_cell(grid, x, y):
	'''Given a grid and the coordinates of a grid cell on that grid, return the left adjacent grid cell of that same grid.'''
	x -= 1
	return get_cell(grid, x, y)

# get the cell to the right of another cell
def get_right_cell(grid, x, y):
	'''Given a grid and the coordinates of a grid cell on that grid, return the right adjacent grid cell of that same grid.'''
	x += 1
	return get_cell(grid, x, y)

# get the cell above of another cell
def get_up_cell(grid, x, y):
	'''Given a grid and the coordinates of a grid cell on that grid, return the up adjacent grid cell of that same grid.'''
	y -= 1
	return get_cell(grid, x, y)

# get the cell below of another cell
def get_down_cell(grid, x, y):
	'''Given a grid and the coordinates of a grid cell on that grid, return the down adjacent grid cell of that same grid.'''
	y += 1
	return get_cell(grid, x, y)

# move a player
def move_player_left(grid, x, y):
	'''Given a grid and the coordinates of a grid cell on that grid, move the player occupying that grid cell to the left adjacent grid cell of that same grid.'''
	try:
		target_cell = get_left_cell(grid, x, y)
		if(target_cell.player != None): raise PlayerTriedToMoveIntoAnotherPlayerError
		swap_players(grid, get_cell(grid, x, y), target_cell)
	except IndexError:
		raise PlayerTriedToGoThroughWallError

# move a player
def move_player_right(grid, x, y):
	'''Given a grid and the coordinates of a grid cell on that grid, move the player occupying that grid cell to the right adjacent grid cell of that same grid.'''
	try:
		target_cell = get_right_cell(grid, x, y)
		if(target_cell.player != None): raise PlayerTriedToMoveIntoAnotherPlayerError
		swap_players(grid, get_cell(grid, x, y), target_cell)
	except IndexError:
		raise PlayerTriedToGoThroughWallError

# move a player
def move_player_up(grid, x, y):
	'''Given a grid and the coordinates of a grid cell on that grid, move the player occupying that grid cell to the up adjacent grid cell of that same grid.'''
	try:
		target_cell = get_up_cell(grid, x, y)
		if(target_cell.player != None): raise PlayerTriedToMoveIntoAnotherPlayerError
		swap_players(grid, get_cell(grid, x, y), target_cell)
	except IndexError:
		raise PlayerTriedToGoThroughWallError

# move a player
def move_player_down(grid, x, y):
	'''Given a grid and the coordinates of a grid cell on that grid, move the player occupying that grid cell to the down adjacent grid cell of that same grid.'''
	try:
		target_cell = get_down_cell(grid, x, y)
		if(target_cell.player != None): raise PlayerTriedToMoveIntoAnotherPlayerError
		swap_players(grid, get_cell(grid, x, y), target_cell)
	except IndexError:
		raise PlayerTriedToGoThroughWallError

# move a player freely
def move_player(grid, x, y, x_movement, y_movement):
	'''Given a grid and the coordinates of a grid cell on that grid, move the player occupying that grid cell x and y number of spaces horizontally and vertically respectively.'''
	h_pos = x_movement >= 0
	v_pos = y_movement >= 0
	h_pol = h_pos and 1 or -1
	v_pol = v_pos and 1 or -1
	h_mag = abs(x_movement)
	v_mag = abs(y_movement)
	h_func = h_pos and move_player_right or move_player_left
	v_func = v_pos and move_player_up or move_player_down
	for x_off in range(h_mag): h_func(grid, x + x_off * h_pol, y)
	for y_off in range(v_mag): v_func(grid, x + x_movement, y - y_off * v_pol)
	
''' example of making a new 7x7 grid and adding a glyph to cell at coordinates 3,4 then printing what glyphs are at those coordinates

grid = make_grid(7, 7)
add_cell_glyph(grid, 3, 4, "some glyph yes")
print(get_cell(grid, 3, 4).glyphs)
'''

''' example of making a new 4x5 grid and adding a player to cell at coordinates 1,0 facing down then printing the id of the direction the player is facing at those coordinates

grid = make_grid(4, 5)
set_cell_player(grid, 1, 0, Player(DOWN))
print(get_cell(grid, 1, 0).player.direction)
'''

# get the distance between two cell coordinates
def get_distance(x1, y1, x2, y2):
	'''Return the physical distance (measured in units of 1 square cell) between two grid cells indicated by a pair of coordinates.'''
	return abs(x2 - x1) + abs(y2 - y1)

# get closest player to a location
def get_closest_player(grid, xo, yo, truesight=False):
	'''Return the player on a grid whose distance from a given location is smallest. Stealth players are ignored unless truesight is True.'''
	import random
	width, height = get_grid_dimensions(grid)
	players = []
	closest = None
	for y in range(width):
		row = grid[y]
		for x in range(height):
			cell = row[x]
			if cell.player and (truesight or not cell.player.is_stealth()):
				distance = get_distance(x, y, xo, yo)
				if closest == None or distance < closest: closest = distance
				players.append({'player': cell.player, 'distance': distance})
	players = [x for x in players if x['distance'] == closest]
	return random.choice(players)['player']


### SVG STUFF ###
grid_size = 32 # pixel size of a grid cell
cell_padding = 4 # pixels of padding inside bounds of grid cell
glyph_cell_padding = 2 # pixels of padding inside bounds of grid cell for glyphs
glyph_margin = 1 # pixels separation of glyph icons
glyph_size = 8 # pixel size of glyph icons
arrow_scale = 0.25 # how much of the full padded cell an arrow occupies

# generate an svg grid cell
def svg_cell(x, y):
	'''Draw a grid cell given the coordinates of the grid cell using SVG.'''
	x_pix = x * grid_size
	y_pix = y * grid_size
	return '''<rect x="''' + str(x_pix) + '''" y="''' + str(y_pix) + '''" width="''' + str(grid_size) + '''" height="''' + str(grid_size) + '''" stroke="blue" fill="white" stroke-width="2"/>'''

# generate an svg arrow in a grid cell
def svg_right_arrow(x, y):
	'''Draw a right arrow given the coordinates of the containing grid cell using SVG.'''
	x_pix = x * grid_size
	y_pix = y * grid_size
	padded_space = grid_size - cell_padding * 2
	tip = grid_size - cell_padding
	tail_offset = -padded_space * arrow_scale
	center = grid_size / 2
	center_offset = padded_space / 2 * arrow_scale
	x2 = x_pix + grid_size - cell_padding
	y2 = y_pix + center
	x1 = x_pix + tip + tail_offset
	y1 = y2 - center_offset
	x3 = x1
	y3 = y2 + center_offset
	return '''<polyline points="''' + str(x1) + ' ' + str(y1) + ' ' + str(x2) + ' ' + str(y2) + ' ' + str(x3) + ' ' + str(y3) + '''" stroke="orange" fill="transparent" stroke-width="2"/>'''

# generate an svg arrow in a grid cell
def svg_left_arrow(x, y):
	'''Draw a left arrow given the coordinates of the containing grid cell using SVG.'''
	x_pix = x * grid_size
	y_pix = y * grid_size
	padded_space = grid_size - cell_padding * 2
	tip = cell_padding
	tail_offset = padded_space * arrow_scale
	center = grid_size / 2
	center_offset = padded_space / 2 * arrow_scale
	x2 = x_pix + tip
	y2 = y_pix + center
	x1 = x_pix + tip + tail_offset
	y1 = y2 - center_offset
	x3 = x1
	y3 = y2 + center_offset
	return '''<polyline points="''' + str(x1) + ' ' + str(y1) + ' ' + str(x2) + ' ' + str(y2) + ' ' + str(x3) + ' ' + str(y3) + '''" stroke="orange" fill="transparent" stroke-width="2"/>'''

# generate an svg arrow in a grid cell
def svg_down_arrow(x, y):
	'''Draw a down arrow given the coordinates of the containing grid cell using SVG.'''
	x_pix = x * grid_size
	y_pix = y * grid_size
	padded_space = grid_size - cell_padding * 2
	tip = grid_size - cell_padding
	tail_offset = -padded_space * arrow_scale
	center = grid_size / 2
	center_offset = padded_space / 2 * arrow_scale
	y2 = y_pix + grid_size - cell_padding
	x2 = x_pix + center
	y1 = y_pix + tip + tail_offset
	x1 = x2 - center_offset
	y3 = y1
	x3 = x2 + center_offset
	return '''<polyline points="''' + str(x1) + ' ' + str(y1) + ' ' + str(x2) + ' ' + str(y2) + ' ' + str(x3) + ' ' + str(y3) + '''" stroke="orange" fill="transparent" stroke-width="2"/>'''

# generate an svg arrow in a grid cell
def svg_up_arrow(x, y):
	'''Draw an up arrow given the coordinates of the containing grid cell using SVG.'''
	x_pix = x * grid_size
	y_pix = y * grid_size
	padded_space = grid_size - cell_padding * 2
	tip = cell_padding
	tail_offset = padded_space * arrow_scale
	center = grid_size / 2
	center_offset = padded_space / 2 * arrow_scale
	y2 = y_pix + tip
	x2 = x_pix + center
	y1 = y_pix + tip + tail_offset
	x1 = x2 - center_offset
	y3 = y1
	x3 = x2 + center_offset
	return '''<polyline points="''' + str(x1) + ' ' + str(y1) + ' ' + str(x2) + ' ' + str(y2) + ' ' + str(x3) + ' ' + str(y3) + '''" stroke="orange" fill="transparent" stroke-width="2"/>'''
	return '''<polyline points="''' + str(x1) + ' ' + str(y1) + ' ' + str(x2) + ' ' + str(y2) + ' ' + str(x3) + ' ' + str(y3) + '''" stroke="orange" fill="transparent" stroke-width="2"/>'''

# output an svg image using an internet url to the image
def svg_image(href, x, y, width, height, mask_id=None):
	'''Draw an image using SVG.'''
#	href = 'file:///' + cwd + '/' + href
	if mask_id: mask_str = '''mask="url(#''' + mask_id + ''')"'''
	else: mask_str = ""
	return '''<image xlink:href="''' + href + '''" x="''' + str(x) + '''" y="''' + str(y) + '''" height="''' + str(height) + '''" width="''' + str(width) + '''" ''' + mask_str + '''/>'''
	
# output an svg player icon circle mask thing yeah
def svg_icon_mask(x, y, size, id):
	'''Draw an SVG mask for a player icon.'''
	radius = size / 2
	cx = x + radius
	cy = y + radius
	return '''<mask id="''' + str(id) + '''"><rect fill="black" x="''' + str(x) + '''" y="''' + str(y) + '''" height="''' + str(size) + '''" width="''' + str(size) + '''"/>''' + '''<circle fill="white" cx="''' + str(cx) + '''" cy="''' + str(cy) + '''" r="''' + str(radius) + '''" /></mask>'''
#	return '''<rect fill="black" x="''' + str(x) + '''" y="''' + str(y) + '''" height="''' + str(size) + '''" width="''' + str(size) + '''"/>''' + '''<circle fill="white" cx="''' + str(cx) + '''" cy="''' + str(cy) + '''" r="''' + str(radius) + '''" />'''
	
# generate a player icon at a grid cell
def svg_icon(pfp_url, x, y):
	'''Draw a player icon given the coordinates of the grid cell of the player and the profile picture URL.'''
	global MASK_COUNT
	x_pix = x * grid_size + cell_padding
	y_pix = y * grid_size + cell_padding
	padded_space = grid_size - cell_padding * 2
	id = "icon_" + str(MASK_COUNT)
	MASK_COUNT += 1
	return svg_icon_mask(x_pix, y_pix, padded_space, id) + svg_image(pfp_url, x_pix, y_pix, padded_space, padded_space, id)
	#return svg_image(pfp_url, x_pix, y_pix, padded_space, padded_space, id)

# generate a glyph icon in a grid cell
def svg_glyph(filepath, x, y, i):
	'''Draw a glyph given the image of the glyph, the coordinates of the containing grid cell, and the index of the glyph in that grid cell's list of glyphs.'''
	glyph_space = glyph_size + glyph_margin
	i_offset = i * glyph_space
	x_pix = x * grid_size + glyph_cell_padding + i_offset
	y_pix = y * grid_size + glyph_cell_padding
	return svg_image(filepath, x_pix, y_pix, glyph_size, glyph_size)

# generate an svg document containing some svg code
def svg_doc(svg, width=200, height=200):
	'''Return SVG code for an SVG document containing more SVG code.'''
	return '''<svg width="''' + str(width) + '''" height="''' + str(height) + '''" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">''' + svg + '''</svg>'''

# generate an svg rendering of a grid
def svg_grid(grid):
	'''Draw a given grid using SVG.'''
	svg = ''
	x = 0
	for row in grid:
		y = 0
		for cell in row:
			# draw the grid cell itself
			svg += svg_cell(x, y)
			# then draw any players that are here
			#try:
				#temp_icon_path = path.join(r'.', 'temp', str(cell.player.user_id) + '.png')
				#temp_icon_path = path.join(r'.', 'temp', str(cell.player.user_id) + '.temp')
			if cell.player is not None:
				temp_icon_path = path.join(r'.', 'temp', str(cell.player.user_id) + '.png')
				if path.exists(temp_icon_path):
					svg += svg_icon(temp_icon_path, x, y)
					if cell.player.direction == 'LEFT':
						svg += svg_left_arrow(x, y)
					elif cell.player.direction == 'RIGHT':
						svg += svg_right_arrow(x, y)
					elif cell.player.direction == 'UP':
						svg += svg_up_arrow(x, y)
					elif cell.player.direction == 'DOWN':
						svg += svg_down_arrow(x, y)
			#except AttributeError: pass
			# draw any glyphs here
			i = 0
			for glyph in cell.glyphs:
				svg += svg_glyph(glyph.icon, x, y, i)
				i += 1
			y += 1
		x += 1
	return svg

# tell all the player objects a render is about to happen and to be ready (icons)
async def pre_render(grid, client):
	'''Tell all player objects on grid that they are about to be rendered.'''
	height, width = get_grid_dimensions(grid)
	for y in range(width):
		row = grid[y]
		for x in range(height):
			cell = row[x]
			if cell.player: await cell.player.pre_render(client)

# render svg code into png data
# optionally output to a file
def render_svg(svg_data, svg_temp_dir, temp_dir):
	'''Given SVG code, render as an image and optionally output the image to a file.'''
	with open(svg_temp_dir, 'w+') as out:
		out.write(svg_data)
	from subprocess import call
	#run(["magick", "convert", svg_temp_dir, temp_dir])
	svg2png(bytestring=svg_data, write_to=temp_dir)

# render a game grid into png data
# optionally output to a file
async def render_grid(grid, client, svg_temp_dir, temp_dir):
	'''Render a given grid to an image file.'''
	await pre_render(grid, client)
	width, height = get_grid_dimensions(grid)
	render_svg(svg_doc(svg_grid(grid), height * grid_size, width * grid_size), svg_temp_dir, temp_dir)

''' example of rendering some shapes to a png file

render_svg(svg_doc(svg_up_arrow(0, 0) + svg_down_arrow(1, 0) + svg_left_arrow(2, 1) + svg_cell(0, 0) + svg_cell(0, 1) + svg_cell(2, 3)), 'out.png')
'''

''' example of rendering a game grid to png file with some players in various directions and some glyphs here and there and then moving some players around
grid = make_grid(2, 2)
set_cell_player(grid, 0, 1, Player(UP, 'hello.png'))
set_cell_player(grid, 1, 0, Player(UP, 'icon.png'))
set_cell_player(grid, 1, 1, Player(RIGHT, 'icon.png'))
render_grid(grid, 'out2.png')
print(get_closest_player(grid, 0, 0))
'''
