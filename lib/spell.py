# Objects MUST be named after the name of the spell, but subclasses MUST be named Spell, like so: class Spell(BaseSpell):
# Fireball = Spell(stuff)
class BaseSpell:
	'''Objects MUST be named after the name of the spell, but subclasses MUST be named Spell, like so: class Spell(BaseSpell):\n
	Fireball = Spell(stuff)'''
	def __init__(self, name, flavor_text, type, mana_cost):
		self.name = name
		self.flavor_text = flavor_text
		self.type = type
		self.mana_cost = mana_cost

	# This is just here to tell you that you need it
	# This is what the bot runs to construct the spell
	def construct(client, message):
		'''This is what the API calls to run the spell.'''
		pass

def calc_damage(base, roll):
	'''Takes the base damage and a roll, then calculates the standard damage output.'''
	return int(base * (0.05 * roll + 0.5))