import pickle
import os
from urllib import request
from cairosvg import svg2png
from discord.utils import get

class Player():
	def __init__(self, user_id, name, gender, age, race, height=None, aura=None):
		self.name = name
		self.gender = gender
		self.age = age
		self.race = race
		self.height = height
		self.aura = aura
		self.user_id = user_id
		self.inv = []
		self.direction = None
		self.in_combat = False
		self.status = {'roll+': 0, 'roll-': 0, 'ceiling': 0, 'adv': False, 'atk_adv': False, 'dodge_adv': False, 'disadv': False, 'atk_disadv': False, 'dodge_disadv': False, 'chance': 0, 'dodge': 0}
		'''I feel like status really needed some proper documentation. Statuses will be added as they are made.\n\n\n


		roll+ = bonuses to end of the roll (e.g. roll+1 would add 1 to each roll, unless overall=True).\n\n
		roll- = debuffs to end of the roll (e.g. roll-1 would subtract 1 to each roll, unless overall=True).\n\n
		ceiling+ = bonuses or debuffs to the d10 damage roll (e.g. ceiling: +1 would allow for a d11, or a total of 105% damage or ceiling: -1 would max the d10 at d9, or a total of 95% damage).\n\n
		adv = Defaults to False. Returns whether the user has advantage or not.\n\n
		atk_adv = Defaults to False. Returns whether the user has advantage on attack rolls or not.\n\n
		dodge_adv = Defaults to False. Returns whether the user has advantage on dodge rolls or not.\n\n
		disadv = Defaults to False. Returns whether the user has disadvantage or not.\n\n
		atk_disadv = Defaults to False. Returns whether the user has disadvantage on attack rolls or not.\n\n
		dodge_disadv = Defaults to False. Returns whether the user has disadvantage on dodge rolls or not.\n\n
		chance = bonus or debuff to chance to hit.\n\n
		dodge = bonus or debuff on chance to dodge.'''
		self.spells = ['Magic Missile']
		self.stats = {'hp': 0, 'current_hp': 0, 'willpower': 0, 'current_willpower': 0,
					'armor': 0, 'current_armor': 0, 'mana_shield': 0, 'current_mana_shield': 0,
					'phy': 0, 'mag': 0, 'reserve': 0, 'current_reserve': 0, 'sense': 0,
					'current_sense': 0, 'control': 0, 'current_control': 0, 'gate': 0,
					'current_gate': 0, 'hp_recovery': 0, 'hp_per_turn': 0, 'mana_recovery': 0,
					'mana_per_turn': 0}
		'''Player's stats. Note that these do NOT include buffs. To get the buffed version use :meth:`Player.calc_stats()`.'''
		self.skills = {'str': 5, 'dex': 5, 'dur': 5, 'wis': 5, 'int': 5, 'util': 5, 'mana': 5}
		self.skill_bonuses = {'str': 0, 'dex': 0, 'dur': 0, 'wis': 0, 'int': 0, 'util': 0}
		self.min = 5
		self.max = 15
		self.percent_buffs = {'hp': 0, 'will': 0, 'armor': 0, 'mana_shield': 0,
					'phy': 0, 'mag': 0, 'reserve': 0, 'sense': 0, 'control': 0, 'gate': 0,
					'hp_recovery': 0, 'hp_per_turn': 0, 'mana_recovery': 0, 'mana_per_turn': 0}
		self.static_buffs = self.percent_buffs
		self.total_points = 30
		self.exp = 0
		self.level = 1
		self.exp_to_levelup = self.admin_update_exp_to_levelup()
		self.current_percent_buffs = self.percent_buffs
		self.current_static_buffs = self.static_buffs
		self.is_turn = False
		self.attacked_on_turn = False
		self.prepared_on_turn = False
		self.attack_counter = 1

	# the grid will call this on player objects before rendering the grid
	async def pre_render(self, client):
		# use it as an excuse to fetch the current player icons
		await self.get_icon(client)
		# TODO MAYBE: cache? detect whether anything has even been changed, and don't waste time downloading it again if it hasn't

	async def get_icon(self, client):
		# file names
		temp_path = os.path.join(r'.', 'temp', str(self.user_id) + '.png')

		# download the dang file
		opener=request.build_opener()
		opener.addheaders=[('User-Agent','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1941.0 Safari/537.36')]
		request.install_opener(opener)
		temp_User = await self.get_user(client)
		request.urlretrieve(temp_User.avatar_url.replace('webp', 'png'), temp_path)

	# Returns player's Discord user object
	async def get_user(self, client):
		return await client.get_user_info(self.user_id)

	# Checked every time a player gains EXP
	async def gain_exp(self, message, client, exp: int, send_messages=False):
		self.exp += exp
		orig_exp = exp
		author = message.author
		while self.exp >= self.exp_to_levelup:
			self.exp_to_levelup = (6 * (self.level ** 3 )) // 3 + 2
			self.level += 1
			self.total_points = 50 + 26 * self.level - sum([self.skills[skill] for skill in self.skills]) - 26
			if send_messages:
				await client.send_message(message.channel, '{} gained {} EXP and leveled up to level {}!'.format(author.mention, exp, self.level))
		if orig_exp < self.exp_to_levelup and send_messages:
			await client.send_message(message.channel, '{} gained {} EXP!'.format(author.mention, exp))
		save_player(self)

	def admin_update_exp_to_levelup(self):
		self.exp_to_levelup = (6 * (self.level ** 3 )) // 3 + 2

	def get_buffed_stats(self):
		'''Calculates the player's stats, including buffs. Returns each stat in a dictionary.\n
		NOTE: Percentages are done BEFORE static buffs.'''
		buffed_stats = self.stats

		for stat in buffed_stats:
			if 'will' not in stat:
				buffed_stats[stat] *= self.current_percent_buffs[stat.replace('current_', '')] // 100 + 1
				buffed_stats[stat] += self.current_static_buffs[stat.replace('current_', '')]
			else:
				buffed_stats[stat] *= self.current_percent_buffs['hp'] // 100 + 1
				buffed_stats[stat] += self.current_static_buffs['hp']

		return buffed_stats

	'''

		Damage functions

	'''
	
	def physical_damage(self, damage, stat='hp'):
		'''Calculates physical damage done to the players, accounting for Armor.\n\n
		
		damage is the damage being done.\n
		stat is if the damage is attacking hp or willpower.'''

		damage_done = damage - self.stats['current_armor']

		if stat == 'hp' and damage_done > 0:
			self.stats['current_hp'] -= damage_done

		elif stat == 'willpower' and damage_done > 0:
			self.stats['current_willpower'] -= damage_done

		return damage_done

	def magical_damage(self, damage, stat='hp'):
		'''Calculates magical damage done to the players, accounting for Mana Shield.\n\n
		
		damage is the damage being done.\n
		stat is if the damage is attacking hp or willpower.'''

		damage_done = damage - self.stats['current_mana_shield']

		if stat == 'hp' and damage_done > 0:
			self.stats['current_hp'] -= damage_done

		elif stat == 'willpower' and damage_done > 0:
			self.stats['current_willpower'] -= damage_done

		return damage_done

	'''

		Update functions

	'''

	def reset_buffs(self):
		self.current_percent_buffs = self.percent_buffs
		self.current_static_buffs = self.static_buffs

	def update_current_stats(self):
		for stat in stats:
			if stat.startswith('current'):
				self.stats[stat] = self.stats[stat.remove('current_')] * (1 + (self.current_percent_buffs[stat] - self.percent_buffs[stat]))

	def buff(self, stat, buff):
		if '.' in buff:
			buff = float(buff)
			self.percent_buffs[stat] += buff
		else:
			buff = int(buff)
			self.static_buffs[stat] += buff

	# When a player levels up they can get access to this
	async def update_skills(self, update_raw, client, message_channel):
		if self.total_points is not 0:
		
			# The update is equivalent to their value in skills, meaning pos 0 is 'str', pos 1 is 'dex', etc.
			update = update_raw.split(':')
			if sum([int(skill) for skill in update]) <= self.total_points:
				i = 0
				skill_max_check = ''
				for skill in self.skills:
					if i == 6969:
						break
					if self.skills[skill] + int(update[i]) > self.max:
						i = 6969
						skill_max_check = skill
					else: i += 1
				if i == 6969:
					await client.send_message(message_channel, f'You added too much to {skill_max_check} and went over the maximum. Please try again.')
				else:
					i = 0
					for skill in self.skills:
						self.skills[skill] += int(update[i])
						i += 1
					self.total_points -= sum([int(skill) for skill in update])
			else: await client.send_message(message_channel, 'You tried to add over your available amount of points. Please try again.')

		else: await client.send_message(message_channel, 'You do not have any available points. Please wait until leveling up or gaining new points.')

	async def update_single_skill(self, skill_to_update, amount_to_update):
		"""Updates a single skill by a specified amount."""
		if self.total_points is not 0:

			if amount_to_update <= self.total_points:
				self.skills[skill_to_update] += amount_to_update
				self.total_points -= amount_to_update

			else: await client.send_message(message_channel, 'You tried to add over your available amount of points. Please try again.')

		else: await client.send_message(message_channel, 'You do not have any available points. Please wait until leveling up or gaining new points.')

	# ALWAYS DO BEFORE update_stats unless in certain currently unknown circumstances!!!
	def update_bonuses(self):
		# Note this came from a Google Sheet. F33 and F34 are cells with a really weird bonus to level check
		# Even I don't remember their exact calculations...
		# I believe it has to do with the average points earned every level
		F33 = 5 + 3 * self.level - 3
		F34 = 15 + 5 * self.level - 5
		for skill in self.skills:
			if self.skills[skill] < (F34 + F33) // 2:
				self.skill_bonuses[skill] = (self.skills[skill] - F33) // 4
			else:
				self.skill_bonuses[skill] = (F34 - F33) // 4

	def update_stats(self):
		for stat in self.stats:
			skill_str = self.skills['str']
			bonus_str = self.skill_bonuses['str']
			skill_dex = self.skills['dex']
			bonus_dex = self.skill_bonuses['dex']
			skill_dur = self.skills['dur']
			bonus_dur = self.skill_bonuses['dur']
			skill_wis = self.skills['wis']
			bonus_wis = self.skill_bonuses['wis']
			skill_int = self.skills['int']
			bonus_int = self.skill_bonuses['int']
			skill_util = self.skills['util']
			bonus_util = self.skill_bonuses['util']
			skill_mana = self.skills['mana']
		
			# If it's a current_hp or something similar stat, gets the origin
			if stat.startswith('current'):
				self.stats[stat] = self.stats[stat.split('current_')[1]]

			# Unless you know what you're doing, these stat updates are pretty much unreadable.
			# Essentially, only change if you have access to the original Google Sheet and know wtf you're doing.
			# All updates to this must also be done on the Google Sheet for readability.
			# Big scary
			else:
				if stat == 'hp':
					self.stats['hp'] = int(((skill_str + bonus_str * 12) * 3 + (skill_dex + bonus_dex * 12) * 2 + (skill_dur + bonus_dur * 12) * 8 + (skill_wis + bonus_wis * 12)) * 2)
				elif stat == 'willpower':
					self.stats['willpower'] = int((skill_str + bonus_str * 12) + (skill_dur + bonus_dur * 12) * 5 + float(skill_util + bonus_util * 12) * 0.1 + (float(skill_mana) * 0.025))
				elif stat == 'armor':
					self.stats['armor'] = int((float((skill_dex + bonus_dex * 12)) * 0.5 + float((skill_dur + bonus_dur * 12)) * 0.5))
				elif stat == 'mana_shield':
					self.stats['mana_shield'] = int(((skill_util + bonus_util * 12) / 2 + (skill_wis + bonus_wis * 12)) // 2)
				elif stat == 'phy':
					self.stats['phy'] = ((skill_str + bonus_str * 16) * 4 + (skill_dex + bonus_dex * 16) * 2) // 2
				elif stat == 'mag':
					self.stats['mag'] = ((skill_int + bonus_int * 16) * 4 + (skill_wis + bonus_wis * 16) * 2) // 2
				elif stat == 'reserve':
					self.stats['reserve'] = int((skill_mana * 8) + float(skill_util + bonus_util * 12) * 0.5 + float(skill_int + bonus_int * 12) * 0.25)
				elif stat == 'sense':
					self.stats['sense'] = int(float(skill_dex + bonus_dex * 12) * 0.5 + float(skill_int + bonus_int * 12) * 0.25 + (skill_util + bonus_util * 12) + float(skill_wis + bonus_wis * 12) * 0.5 + (float(skill_mana) * 0.25))
				elif stat == 'control':
					self.stats['control'] = int((skill_dex + bonus_dex * 12) + float(skill_dur + bonus_dur * 12) * 0.25 + float(skill_int + bonus_int * 12) * 0.5 + float(skill_util + bonus_util * 12) * 0.5 + float(skill_wis + bonus_wis * 12) * 0.15 + (float(skill_mana) * 0.1))
				elif stat == 'gate':
					self.stats['gate'] = int(((float(skill_mana) * 0.25) + float(skill_str + bonus_str * 12) * 0.05 + float(skill_dur + bonus_dur * 12) * 0.025 + float(skill_int + bonus_int * 12) * 0.05 + float(skill_util + bonus_util * 12) * 0.1 + float(skill_wis + bonus_wis * 12) * 0.05) * 4)
				elif stat == 'hp_recovery':
					self.stats['hp_recovery'] = (skill_dur + bonus_dur * 12) * 5 + (skill_util + bonus_util * 12) // 2 + (skill_wis + bonus_wis * 12) * 2
				elif stat == 'hp_per_turn':
					self.stats['hp_per_turn'] = self.stats['hp_recovery'] // 20
				elif stat == 'mana_recovery':
					self.stats['mana_recovery'] = int((skill_dur + bonus_dur * 12) * 0.25 + (skill_util + bonus_util * 16) + (skill_wis + bonus_wis * 10))
				elif stat == 'mana_per_turn':
					self.stats['mana_per_turn'] = self.stats['mana_recovery'] // 3

	def __str__(self):
		return '**ID:** {}\n**Name:** {}\n**Level:** {}\n**EXP:** {}\n**EXP to Level Up:** {}\n**Inventory:** {}\n**Status:** {}\n**Spells:** {}\n**Stats:** {}\n**Skills:** {}\n**Skill Bonuses:** {}\n**Percent Buffs:** {}\n**Static Buffs:** {}\n'.format(self.user_id,
																																																	self.name, self.level,
																																																	self.exp, self.exp_to_levelup,
																																																	self.inv, self.status, self.spells,
																																																	self.stats, self.skills,
																																																	self.skill_bonuses, self.percent_buffs,
																																																	self.static_buffs)

# Saves the player's data into their .dat
def save_player(Player_object):
		test_path = os.path.join(r'.', 'users', str(Player_object.user_id))
		if not os.path.exists(test_path):
			os.makedirs(test_path)
		with open('./users/{}/player.dat'.format(Player_object.user_id), 'wb+') as f:
			f.write(pickle.dumps(Player_object))

# Returns the player object from the player's id (user.id)
def load_player(player_id):
	try:
		test_path = os.path.join(r'.', 'users', str(player_id), 'player.dat')
		with open(test_path, 'rb') as f:
			return pickle.loads(f.read())
	except OSError:
		return None

# The entire character creation process
async def create_character(message, client):
	race = ''
	while race != None:
		user = message.author
		name = ''
		gender = ''
		age = 0
		race = ''
		height = None
		aura = None
		msg = 'c'

		await client.send_message(message.author, 'Hello, and welcome to Re:Born!\n\nTo begin character creation, please type &setname first_name, family_name, gender, age')
		
		while msg != None:
			msg = await client.wait_for_message(timeout=300, author=user, check=lambda message: message.content.startswith('&setname '))
			create_char = msg.content.replace('&setname ', '')
			create_char = create_char.split(', ')
			name = create_char[0] + ' ' + create_char[1]
			gender = create_char[2]
			if int(create_char[3]) >= 16:
				age = int(create_char[3])
				break
			else:
				await client.send_message(message.author, 'There was an error in creating your character. Most likely, it is your age. Your character must be at least 16 years old.')
		if create_char == 'c': break

		await client.send_message(message.author, 'Please now choose a race. Put your selection as your next message. Keep in mind that capitalization does not count but if your race is not in the race tree it will be invalid. (will be fixed after pre-alpha)')
		race = await client.wait_for_message(timeout=3000, author=user)
		race = race.content

		msg = 'c'
		while msg != None:
			height = None
			aura = None
			await client.send_message(message.author, 'You can optionally add your height and a flavor text of your aura. For height, do &height feet\'inches", and for aura do &aura flavor text. To skip, type &skip.')
			msg = await client.wait_for_message(timeout=3000, author=user, check=lambda message: message.content.startswith('&height ') or message.content.startswith('&aura ') or message.content.startswith('&skip'))
			if msg.content.startswith('&height '):
				height = msg.content.replace('&height ', '')
			elif msg.content.startswith('&aura '):
				aura = msg.content.replace('&aura ', '')
			elif msg.content.startswith('&skip'):
				break

		await client.send_message(message.author, 'Now it is time for your stats! At any time press the :notebook: emoji to see your stats! You have 30 points to distribute among your skills! Update your skills by clicking their respective emojis.\n')
		msg = 'c'
		total_points = 35
		Player_object = Player(user.id, name, gender, age, race, height, aura)
		emoji_check = {'\U00002694': 'str', '\U000023E9': 'dex', '\U0001F6E1': 'dur', '\U0001F4D5': 'int', '\U0001F4D7': 'wis',
						'\U0001F4D8': 'util', '\U0001F300': 'mana'}
		emoji_check_2 = {'⚔️': 'atk', '⏩': 'dex', '🛡️': 'dur', '📕': 'int', '📗': 'wis',
						'📘': 'util', '🌀': 'mana'}

		Player_object.update_bonuses()
		Player_object.update_stats()
		skill_str = ''
		for skill, amount in Player_object.skills.items():
			skill_str += '**{}**: {}\n'.format(skill, amount)
		await client.send_message(user, skill_str)

		stat_str = ''
		for stat, amount in Player_object.stats.items():
			if 'current' not in stat:
				stat_str += '**{}**: {}\n'.format(stat, amount)
		await client.send_message(user, stat_str)

		positive = True
		
		while total_points != 0:
			emojis_to_react = ['\U0001F4D3', '\U00002694', '\U000023E9', '\U0001F6E1', '\U0001F4D5', '\U0001F4D7',
							'\U0001F4D8', '\U0001F300', positive and '➕' or '➖']
			
			message_to_wait = await client.send_message(message.author, 'Min: {}\nMax: {}\nPoints Left: {}\n\n'.format(Player_object.min, Player_object.max, Player_object.total_points) +
													':notebook: Stats\n'
													':heavy_plus_sign: Positive (meaning you are adding one every time)\n'
													':heavy_minus_sign: Negative (meaning you are removing one every time)\n'
													':crossed_swords: ATK - **{}**\n'.format(Player_object.skills['str']) +
													':fast_forward: DEX - **{}**\n'.format(Player_object.skills['dex']) +
													':shield: DUR - **{}**\n'.format(Player_object.skills['dur']) +
													':closed_book: INT - **{}**\n'.format(Player_object.skills['int']) +
													':green_book: WIS - **{}**\n'.format(Player_object.skills['wis']) +
													':blue_book: UTIL - **{}**\n'.format(Player_object.skills['util']) +
													':cyclone: MANA - **{}**'.format(Player_object.skills['mana']))
			for emoji in emojis_to_react:
				await client.add_reaction(message_to_wait, emoji)

			skill_upgrade = ''

			skill_upgrade = await client.wait_for_reaction(emojis_to_react, user=user, timeout=3000, message=message_to_wait)

			if str(skill_upgrade.reaction.emoji) == '\U0001F4D3': # Notebook
				Player_object.update_bonuses()
				Player_object.update_stats()
				stat_str = ''
				for stat, amount in Player_object.stats.items():
					if 'current' not in stat:
						stat_str += '**{}**: {}\n'.format(stat, amount)
				await client.send_message(user, stat_str)

			elif str(skill_upgrade.reaction.emoji) == '➕': # Plus sign
				positive = False

			elif str(skill_upgrade.reaction.emoji) == '➖': # Minus sign
				positive = True

			else:
				await Player_object.update_single_skill(emoji_check[str(skill_upgrade.reaction.emoji)], positive and 3 or -3)

			total_points = Player_object.total_points

		if skill_upgrade is not None:
			await client.send_message(message.author, f'Congrats! Your character {Player_object.name} is complete! Have fun roleplaying!')
			Player_object.update_bonuses()
			Player_object.update_stats()
			Player_object.admin_update_exp_to_levelup()
			save_player(Player_object)
			await client.add_roles(user, get(message.server.roles, name='Reincarnated Spirit'))
		break