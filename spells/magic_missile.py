import sys

sys.path.append('..')

from lib.spell import BaseSpell, calc_damage
from lib.player import load_player, save_player
from lib.diceroller import auto_roll, does_hit
from main import init_dict

# Instead of using the name auto_roll, you can do the following:
# from lib.diceroller import auto_roll as roll

'''
	Magic Missile is a spell that shoots missiles based on your level, and each one gains damage based on your intelligence.
	Very versatile and powerful spell, but due to the multiple rolls, static debuffs harm it greatly.

	Magic Missile also does not have its own inherent damage calculation. Its damage is completely relient on the caster's mag, or Magic Attack.
'''

class Magic_Missile_Spell(BaseSpell):
	# Had to override due to needing Caster.level for Mana cost
	def __init__(self, name, type, flavor_text):
		self.name = name
		self.type = type
		self.mana_cost = 0
		self.flavor_text = flavor_text

	# Only asynchronous in case people want to use it in the future for messages
	async def update_mana_cost(self, message, client):
		Caster = load_player(message.author.id)
		self.mana_cost = 10 * Caster.level

	@staticmethod
	async def construct(message, client):

		# Building the object and variables for later
		Caster = load_player(message.author.id)
		damage_buff = Caster.skills['int'] * 2 + Caster.status['roll+']
		dice_type = 10 + Caster.status['ceiling']
		buffed_stats = Caster.get_buffed_stats()

		# Magic Missile allows the caster to target multiple enemies. If done so, the enemies will have the missiles equally distributed among them.
		Enemies = message.mentions

		# attack_number is used as an iterator instead of attack so it can wrap around if it's greater than the length of Enemies
		attack_number = 0
		for attack in range(Caster.level):

			# Checking to make sure that the attack_number isn't greater than or equal to the length of enemies
			if attack_number >= len(Enemies):
				attack_number = 0

			Enemy = load_player(Enemies[attack_number-1].id)

			# Checks whether the attack actually hits or not
			hit = does_hit(message.channel.id, Caster, Enemy)

			if hit:

				# Roll for the damage
				roll = auto_roll(1, dice_type, add = damage_buff, sub = Caster.status['roll-'], adv = Caster.status['adv'], disadv = Caster.status['disadv'])
				damage = calc_damage(buffed_stats['mag'], roll)

				# As per usual, please try to keep the same name as the variable that the function returns
				damage_done = Enemy.magical_damage(damage)
				save_player(Enemy)

				# Flavor text
				await client.send_message(message.channel, '*As missile %i zips through the air, it hits its target %s and does %i damage!*' % (attack+1, Enemies[attack_number].mention, damage_done))
				attack_number += 1

			else:
				await client.send_message(message.channel, '*The missile flies uncontrollably through the air, and misses %s*' % (Enemies[attack_number].mention))
				attack_number += 1

flavor_text = ("A common, yet powerful spell. It's a spell that grows with the user, the stronger they are, the stronger the spell.\n\n"
			"Multiple, magical missiles fly through the air, aimed right at their enemies. The path of these missiles are difficult to track, but to due to their size, that doesn't "
			"pose such an issue. The skilled swordsman or mage can still block the missile with relative ease.\n\n"
			"It's said that a strong and genius mage can even control the flight paths of each individual missile. One legendary mage in the days of old could even summon hundreds upon hundreds "
			"of missiles, and control each of their paths individually.")

Spell = Magic_Missile_Spell('Magic Missile', 'main', flavor_text)