.. Re:Born documentation master file, created by
   sphinx-quickstart on Fri Oct 26 10:16:31 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Re:Born's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`



Module: lib.grid
=====================================

.. autoclass:: grid.PlayerTriedToGoThroughWallError
   :members:
.. autoclass:: grid.PlayerTriedToMoveIntoAnotherPlayerError
   :members:
.. autoclass:: grid.Player
   :members:
.. autoclass:: grid.Glyph
   :members:
.. autoclass:: grid.TestGlyph
   :members:
.. autoclass:: grid.TestGlyph2
   :members:
.. autoclass:: grid.Cell
   :members:
.. autofunction:: grid.make_grid
.. autofunction:: grid.get_grid_dimensions
.. autofunction:: grid.get_cell
.. autofunction:: grid.get_player_cell
.. autofunction:: grid.get_player_position
.. autofunction:: grid.set_cell_player
.. autofunction:: grid.set_cell_glyphs
.. autofunction:: grid.add_cell_glyph
.. autofunction:: grid.clear_cell_player
.. autofunction:: grid.clear_cell_glyphs
.. autofunction:: grid.swap_players_coordinates
.. autofunction:: grid.swap_players
.. autofunction:: grid.get_left_cell
.. autofunction:: grid.get_right_cell
.. autofunction:: grid.get_up_cell
.. autofunction:: grid.get_down_cell
.. autofunction:: grid.move_player_left
.. autofunction:: grid.move_player_right
.. autofunction:: grid.move_player_up
.. autofunction:: grid.move_player_down
.. autofunction:: grid.move_player
.. autofunction:: grid.get_distance
.. autofunction:: grid.get_closest_player
.. autofunction:: grid.svg_cell
.. autofunction:: grid.svg_right_arrow
.. autofunction:: grid.svg_left_arrow
.. autofunction:: grid.svg_down_arrow
.. autofunction:: grid.svg_up_arrow
.. autofunction:: grid.svg_image
.. autofunction:: grid.svg_icon_mask
.. autofunction:: grid.svg_icon
.. autofunction:: grid.svg_glyph
.. autofunction:: grid.svg_doc
.. autofunction:: grid.svg_grid
.. autofunction:: grid.pre_render
.. autofunction:: grid.render_svg
.. autofunction:: grid.render_grid
