from sys import path
path.append('..')

from lib.player import save_player, load_player
from lib.grid import move_player, get_player_cell
from main import init_dict

class Move_Command:
	def __init__(self, name, type, description):
		self.name = name
		self.type = type
		self.description = description

	@staticmethod
	def run(message, client):
		Player_object = load_player(message.author.id)
		msg = message.content.remove('!move ')
		find_player = get_player_cell(init_dict['grid'], Player_object)
		move_player(init_dict['grid'], find_player[0], find_player[1], msg.split(':')[0], msg.split(':')[1])
		save_player(Player_object)

Command = Move_Command('!move', 'Grid', 'Moves the player a certain amount of x and y coordinates.\n**Examples:**\n!move 5:10 - moves you 5 to the right, 10 up.\n!move 5:-10 - moves you 5 to the right, 10 down.')