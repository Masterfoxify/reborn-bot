import sys

sys.path.append('../')

from lib.player import load_player, save_player

class GiveXP_Command:
	def __init__(self, name, type, description):
		self.name = name
		self.type = type
		self.description = description

	@staticmethod
	async def run(message, client):
		if 'Admin' in [role.name for role in message.author.roles]:
			Player_object = load_player(message.content.split(' ')[1].split(':')[0])
			await Player_object.gain_exp(message, client, int(message.content.split(': ')[1].replace(':', ''), False))
			save_player(Player_object)
			await client.send_message(message.channel, '{} has been given {} exp.'.format(Player_object.name, message.content.split(': ')[1].replace(':', '')))
		else:
			await client.send_message(message.channel, 'You do not have access to this command.')

Command = GiveXP_Command('!givexp', 'Debug', 'For testing purposes only, and only for use by Admins and higher.')