from sys import path
path.append('..')

from lib.player import load_player

class GetPlayer_Command:
	def __init__(self, name, type, description):
		self.name = name
		self.type = type
		self.description = description

	@staticmethod
	async def run(message, client):
		if 'Admin' in [role.name for role in message.author.roles]:
			user_id = message.content.split(' ')[1]
			Player_object = str(load_player(user_id))
			if Player_object != None:
				await client.send_message(message.channel, Player_object)
			else:
				await client.send_message(message.channel, f'User {message.author.name} has not created a character.')
		else:
			await client.send_message(message.channel, 'You do not have access to this command.')

Command = GetPlayer_Command('!getplayer', 'Debug', 'Only for use by Admins and higher.')