import sys

sys.path.append('../')

from lib.player import load_player, save_player

class AddSpell_Command:
	def __init__(self, name, type, description):
		self.name = name
		self.type = type
		self.description = description

	@staticmethod
	async def run(message, client):
		if 'Admin' in [role.name for role in message.author.roles]:
			Player_object = load_player(message.content.split(' ')[1].split(':')[0])
			spell_name = message.content.split(': ')[1]
			if spell_name not in Player_object.spells:
				Player_object.spells.append(spell_name)
				save_player(Player_object)
				await client.send_message(message.channel, f'Spell "{spell_name}" was added to {Player_object.name}\'s known spells.')
			else:
				await client.send_message(message.channel, f'{Player_object.name} already has the spell in their known spells.')
		else:
			await client.send_message(message.channel, 'You do not have access to this command.')

Command = AddSpell_Command('!addspell', 'Debug', 'For testing purposes only, and only for use by Admins and higher.')