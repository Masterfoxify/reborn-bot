from sys import path
path.append('..')

from lib.grid import make_grid
from main import init_dict

class CreateGrid_Command:
	def __init__(self, name, type, description):
		self.name = name
		self.type = type
		self.description = description

	@staticmethod
	async def run(message, client):
		'''Calls :meth:`grid.make_grid(width=10, height=10)` to create a grid at init_dict[ctx.message.channel.id]'''
		msg = message.content
		msg = msg.replace('!creategrid ', '')
		size_x = int(msg.split(';')[0])
		size_y = int(msg.split(';')[1])
		init_dict[message.channel.id]['grid'] = make_grid(width=size_x, height=size_y)
		await client.send_message(message.channel, f'Created grid size {size_x} by {size_y} in {message.channel.mention}.')

Command = CreateGrid_Command('!creategrid', 'Grid', 'Creates a grid for the initiative with the given size (x, y).\n**Example:** !creategrid 10;10 (creates a 10x10 grid. First number is x, second is y)')