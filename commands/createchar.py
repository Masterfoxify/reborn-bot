from sys import path
path.append('..')

from lib.player import create_character

class CreateChar_Command:
	def __init__(self, name, type, description):
		self.name = name
		self.type = type
		self.description = description

	@staticmethod
	async def run(message, client):
		await create_character(message, client)

Command = CreateChar_Command('!createchar', 'Character', 'Starts the character creation process.\n**Example:** !createchar (will send a PM starting the character creation)')