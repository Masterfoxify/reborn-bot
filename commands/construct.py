from sys import path
path.append('..')

from lib.player import load_player, save_player
from importlib import import_module
from os import listdir

class Construct_Command:
	def __init__(self, name, type, description):
		self.name = name
		self.type = type
		self.description = description

	@staticmethod
	async def run(message, client):
		Player_object = load_player(message.author.id)
		spell_name = message.content.replace('!construct ', '')

		if ',' in spell_name:
			spell_name = spell_name.split(',')[0]
			spell_name = spell_name.replace(' ', '_')
			spell_name = spell_name.lower()

			try:
				spell_to_import = import_module(f'spells.{spell_name}')
				Spell = getattr(spell_to_import, "Spell")

				if Spell.name in Player_object.spells:
					if Spell.mana_cost != 0:
						if Spell.mana_cost > Player_object.stats['current_reserve']:
							await client.send_message(message.channel, f'{Player_object.name} currently does not have the Mana to cast {Spell.name}.')

						else:
							Player_object.stats['current_reserve'] -= Spell.mana_cost
							await Spell.construct(message, client)

					else:
						await Spell.update_mana_cost(message, client)
						if Spell.mana_cost > Player_object.stats['current_reserve']:
							await client.send_message(message.channel, f'{Player_object.name} currently does not have the Mana to cast {Spell.name}.')

						else:
							Player_object.stats['current_reserve'] -= Spell.mana_cost
							await Spell.construct(message, client)

				else:
					await client.send_message(message.channel, 'You do not have the {} spell in your spell list.'.format(Spell.name))

			except ModuleNotFoundError:
				working_name = message.content.replace('!construct ', '').split(',')[0]
				await client.send_message(message.channel, f'{message.author.mention}, **{working_name}** is not a valid spell.')

		else:
			await client.send_message(message.channel, f'{message.author.mention}, make sure you are using a comma to separate your spell name and targets')
		

Command = Construct_Command('!construct', 'Spell', 'If you have the spell, and enough Mana to cast it, then this command will activate the spell.\n**Example:** !construct Magic Missile, @Bob (will shoot missiles at Bob, poor Bob)')