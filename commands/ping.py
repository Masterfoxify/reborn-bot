class Ping_Command:
	def __init__(self, name, type, description):
		self.name = name
		self.type = type
		self.description = description

	@staticmethod
	async def run(message, client):
		print('Pong')
		await client.send_message(message.channel, 'Pong')

Command = Ping_Command('!ping', 'Debug', 'Sends a pong.')