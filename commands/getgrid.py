from sys import path
from os import path as p
from os import remove
path.append('..')
import io
from contextlib import redirect_stdout

from lib.grid import render_grid, get_all_player_ids
from main import init_dict

class GetGrid_Command:
	def __init__(self, name, type, description):
		self.name = name
		self.type = type
		self.description = description

	@staticmethod
	async def run(message, client):
		'''Renders the grid by calling :meth:`grid.render_grid(grid, outfile=None)` and sending it to the channel of origin.'''
		temp_svg_path = p.join(r'.', 'temp', str(message.channel.id) + '.svg')
		temp_png_path = p.join(r'.', 'temp', str(message.channel.id) + '.png')
		await render_grid(init_dict[message.channel.id]['grid'], client, temp_svg_path, temp_png_path)
		await client.send_file(message.channel, temp_png_path)
		remove(temp_png_path)
		remove(temp_svg_path)
		player_ids = get_all_player_ids(init_dict[message.channel.id]['grid'])
		for id in player_ids:
			temp_png = p.join(r'.', 'temp', str(id) + '.png')
			remove(temp_png)

Command = GetGrid_Command('!getgrid', 'Grid', 'Sends a picture of the current state of the grid.\n**Example:** !getgrid')