from sys import path
path.append('..')

from lib.player import load_player, save_player, Player

class Reset_Command:
	def __init__(self, name, type, description):
		self.name = name
		self.type = type
		self.description = description

	@staticmethod
	async def run(message, client):
		if 'Admin' in [role.name for role in message.author.roles]:
			Player_object = load_player(message.content.split(' ')[1])
			New_Player_object = Player(message.author.id, Player_object.name, Player_object.gender, Player_object.age, Player_object.race, Player_object.height, Player_object.aura)
			New_Player_object.update_bonuses()
			New_Player_object.update_stats()
			New_Player_object.admin_update_exp_to_levelup()
			save_player(New_Player_object)
			await client.send_message(message.channel, f'Character {Player_object.name} has been reset.')
		else:
			await client.send_message(message.channel, 'You do not have access to this command.')

Command = Reset_Command('!reset', 'Debug', 'Resets a player\'s data. For testing and for admin use only.')