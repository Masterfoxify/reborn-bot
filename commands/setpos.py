from sys import path
path.append('..')

from lib.player import save_player, load_player
from lib.grid import set_cell_player, get_player_position, clear_cell_player
from main import init_dict

class SetPos_Command:
	def __init__(self, name, type, description):
		self.name = name
		self.type = type
		self.description = description

	@staticmethod
	async def run(message, client):
		'''Sets the player's position on the grid. Will probably be depreciated or reworked for backend due to possibility of cheating, or allowing it only for right after initiative, and before anyone has a turn.
	
Calls: :meth:`grid.set_cell_player(grid, x, y, player)`, then :meth:`player.save_player(Player_object)` to save the player's direction if it has changed.
	
Example usage:
!setpos 10:10;UP
!setpos 5:2;DOWN'''
		temp_Grid = init_dict[message.channel.id]['grid']
		msg = message.content.replace('!setpos ', '')
		Player_object = load_player(message.author.id)
		if ':' in msg:
			uppertest = ['UP', 'DOWN', 'LEFT', 'RIGHT']
			if (msg.split(':')[1]).upper() in uppertest:
				Player_object.direction = msg.split(':')[1].upper()
				save_player(Player_object)

		player_test = get_player_position(temp_Grid, Player_object)
		if player_test is not None: clear_cell_player(temp_Grid, player_test[0], player_test[1])

		_test = ':' in msg
		_test_successful = '.'
		if _test:
			_test_successful = f' and faced **{Player_object.name}** {Player_object.direction.lower()}wards.'
		move_x = int(msg.split(';')[0]) - 1
		move_y = int(msg.split(';')[1].split(':')[0]) - 1
		set_cell_player(temp_Grid, move_x, move_y, Player_object)
		await client.send_message(message.channel, f'Set **{Player_object.name}** at {move_x + 1}, {move_y + 1} in the grid in {message.channel.mention}{_test_successful}')

Command = SetPos_Command('!setpos', 'Grid', 'Sets the player\'s position on the grid, and optionally .\n**Examples:**\n!setpos 10;10 - sets position at x of 10 and y of 10\n!setpos 5;5:DOWN - sets position at x of 5 and y of 5, then faces the player downwards.')