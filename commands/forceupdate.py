import sys

sys.path.append('../')

from lib.player import load_player, save_player

class ForceUpdate_Command:
	def __init__(self, name, type, description):
		self.name = name
		self.type = type
		self.description = description

	@staticmethod
	async def run(message, client):
		if 'Admin' in [role.name for role in message.author.roles]:
			msg = message.content.replace('!forceupdate ', '')
			Player_object = load_player(msg)
			Player_object.update_bonuses()
			Player_object.update_stats()
			Player_object.admin_update_exp_to_levelup()
			save_player(Player_object)
			await client.send_message(message.channel, 'Update complete to users.{}.player.dat'.format(Player_object.user_id))
		else:
			await client.send_message(message.channel, 'You do not have access to this command.')

Command = ForceUpdate_Command('!forceupdate', 'Debug', 'Only for use by Admins and higher.')