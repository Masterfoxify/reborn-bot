from sys import path
path.append('..')

from lib.diceroller import roll_through_message

class Roll_Command:
	def __init__(self, name, type, description):
		self.name = name
		self.type = type
		self.description = description

	@staticmethod
	async def run(message, client):
		'''Takes :meth:`main.roll_through_message(ctx)` and sends results to the channel of message origin.'''
		rolls_and_sum = roll_through_message(message)
		if ' d' not in message.content and '!roll 1d' not in message.content:
			await client.send_message(message.channel, '{}, your total amount {}was: {}'.format(message.author.mention, '^' in message.content and 'overall ' or '', rolls_and_sum['sum']))
			await client.send_message(message.channel, 'And your individual rolls {}were: '.format('>' in message.content and 'with advantage ' or ('<' in message.content and 'with disadvantage ' or '')) + str(rolls_and_sum['rolls']))
		else:
			await client.send_message(message.channel, '{}, you rolled a {}!'.format(message.author.mention, rolls_and_sum))

Command = Roll_Command('!roll', 'Dice', ('Allows players to roll through the bot instead of through a spell or API.\n'
					   '**Example:** !roll 10d10 (would roll 10 d10s)\n'
					   '**Extra Info:** Adding specific symbols can add different effects to the dice rolling. A + or - after the d(number) will allow you to add a specific amount to every roll. '
					   'Adding a > after the roll will give the roll advantage and adding a < after the roll will give the roll disadvantage (greater than = Advantage, Less than = Disadvantage). '
					   'Adding a ^ after the roll will make the addition/subtraction overall, meaning it only adds at the end, and not on every roll.\n'
					   '**Extra Examples:**\n'
					   '!roll 10d10+10 - rolls 10 d10s, adding 10 to each roll.\n' 
					   '!roll 10d10< - rolls 10 d10s, giving advantage, meaning the results are rolled twice, and the largest amount is what\'s presented.\n'
					   '!roll 10d10-10>^ - rolls 10 d10s, subtracting 10 overall, and giving disadvantage, meaning the results are rolled twice and the smallest amount is what\'s presented.'))