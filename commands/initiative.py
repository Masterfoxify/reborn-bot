from sys import path
path.append('..')

from lib.player import load_player, save_player
from lib.diceroller import roll_through_message
from lib.grid import make_grid
from main import init_dict

class Initiative_Command:
	def __init__(self, name, type, description):
		self.name = name
		self.type = type
		self.description = description

	@staticmethod
	async def run(message, client):
		'''Rolls initiative and adds user to init_dict at the channel id of origin. If the channel id does not have an initiative stack, it creates one.'''
		rolls_and_sum = roll_through_message(message)

		try: test_amount = len(rolls_and_sum['rolls'])
		except TypeError: test_amount = 1

		if test_amount is 1:
			Player_object = load_player(message.author.id)

			if Player_object.in_combat == False:
				try: await client.send_message(message.channel, '{} rolled an initiative of {}'.format(Player_object.name, rolls_and_sum['sum']))
				except TypeError: await client.send_message(message.channel, f'{Player_object.name} rolled an initiative of {rolls_and_sum}')

				if message.channel.id not in init_dict:

					# Creates a GRID object using the grid.py module
					init_dict[message.channel.id] = {'grid': make_grid(width=10, height=10), 'init': {}}
					await client.send_message(message.channel, 'Created a new initiative set.')

					try:
						init_dict[message.channel.id]['init'][Player_object.user_id] = rolls_and_sum['rolls'][0]

					except TypeError:
						init_dict[message.channel.id]['init'][Player_object.user_id] = rolls_and_sum

					await client.send_message(message.channel, f'Added {Player_object.name} to the initiative set.')
					# TODO: add algorithm to sort stack in proper order (highest to lowest)

				else:
					try:
						init_dict[message.channel.id][Player_object.user_id] = rolls_and_sum['rolls'][0]
					except TypeError:
						init_dict[message.channel.id][Player_object.user_id] = rolls_and_sum
					Player_object.in_combat = True
					save_player(Player_object)
					await client.send_message(message.channel, f'Added {Player_object.name} to the initiative set.')

			else:
				await client.send_message(message.channel, f'**{Player_object.name}** is already in combat.')

		else:
			await client.send_message(message.channel, f'Error, {Player_object.name} attempted to roll multiple dice on an initiative.')

Command = Initiative_Command('!initiative', 'Grid', 'Rolls initiative and adds user to init_dict at the channel id of origin. If the channel id does not have an initiative stack, it creates one.\n**Example:** !initiative d20')