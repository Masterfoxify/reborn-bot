from sys import path
path.append('..')

from lib.player import save_player, load_player

class Turn_Command:
	def __init__(self, name, type, description):
		self.name = name
		self.type = type
		self.description = description

	@staticmethod
	async def run(message, client):
		Player_object = Player_object = load_player(message.author.id)
		msg = message.content
		uppertest = ['UP', 'DOWN', 'LEFT', 'RIGHT']
		if (msg.split(' ')[1]).upper() in uppertest:
			Player_object.direction = (msg.split(' ')[1]).upper()
			save_player(Player_object)
		else: await client.send_message(message.channel, 'Please type either UP, DOWN, LEFT, or RIGHT.')

Command = Turn_Command('!turn', 'Character', 'Changes the facing of your character on the grid.\n**Example:** !turn DOWN (faces your character downwards)')