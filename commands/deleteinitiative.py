from sys import path
path.append('..')

from lib.player import save_player, load_player
from main import init_dict

class DeleteInitiative_Command:
	def __init__(self, name, type, description):
		self.name = name
		self.type = type
		self.description = description

	@staticmethod
	async def run(message, client):
			'''Deletes initiative, after resetting player buffs.'''
			for player_id in init_dict[message.channel.id]['init']:
					Player_object = load_player(player_id)
					Player_object.reset_buffs()
					save_player(Player_object)
			del init_dict[message.channel.id]
			await client.send_message(message.channel, 'The initiative in {} has been deleted!'.format(message.channel.mention))

Command = DeleteInitiative_Command('!deleteinitiative', 'Grid', 'Deletes initiative of the channel, including the entire grid.\n**Example:** !deleteinitiative')