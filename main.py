from discord import Client, Game
from importlib import import_module, invalidate_caches
from time import sleep
import commands
from os import listdir
from math import ceil

from lib import player
from lib import diceroller
from lib import grid

# Evil globals, except actually really useful and necessary.
init_dict = {}

client = Client()

@client.event
async def on_ready():
	await client.change_presence(game=Game(name='Type !help for help!'))

@client.event
async def on_message(message):
	message.content = message.content.replace('#', '')

	# Doing the "Help" command by checking all commands in the commands package.
	if message.content == '!help':
		types = {}

		# Iterating through every command, checking to make sure it's not pycache.
		for command in listdir('commands'):
			if '__' not in command:
				invalidate_caches()
				command_import = import_module('commands.{}'.format(command.replace('.py', '')))
				if command_import.Command.type not in types:
					types[command_import.Command.type] = [command_import.Command]
				else:
					types[command_import.Command.type].append(command_import.Command)

		# Creating the help screen for the users. Instead of slowing the bot down by sending them in a ton of messages, it combines them together.
		help_screen = ''

		# Iterating through each command type
		for type in types.keys():

			# The next portions are purely Discord bold and italic aesthetics
			help_screen += f'\n\n__**{type}:**__\n\n'
			for Command in types[type]:
				help_screen += f'__{Command.name}__\n----------------------------------------------------------------\n{Command.description}\n\n'

		# So message doesn't overflow the limit
		help_screen_list = []
		if len(help_screen) > 1900:
			amount_over = int(ceil(len(help_screen) / 1900))

			# Iterating through every 1900 characters, then appending them in a splitlines()-esque manner.
			for x in range(1, amount_over+1):
				help_screen_list.append(help_screen[(x - 1) * 1900 : x * 1900])
			for i in help_screen_list:
				await client.send_message(message.author, i)
		else:
			await client.send_message(message.author, help_screen)
	command_name = message.content.split(' ')[0].replace('!', '')

	# Checking to see if the command exists.
	if command_name + '.py' in listdir('commands'):
		invalidate_caches()
		command_import = import_module(f'commands.{command_name}')

		# Running the command's run function.
		await command_import.Command.run(message, client)
	elif message.content.startswith('!') and message.content != '!help':
		await client.send_message(message.channel, 'Invalid command. Please try again.')

# This has to do with imports, making sure there isn't a second instance of the client for importing the init_dict.
if __name__ == '__main__':
	try:
		with open('token.txt', 'r') as token_file:
			client.run(token_file.read().strip())
	except FileNotFoundError:
		print('Please create a file called "token.txt" containing the login token for the Discord bot.')